function [X,Y,dY]=finesol(y,a,t,k)
%% FINESOL: find fem solution and its derivative on fine mesh 
%  y(1:N)    = fem solution 
%  a(1:n)    = f.e. boundary points
%  t(1:m+1)  = grid points on basic element [0,1]
%  k+1       = number of fine mesh points on each f.e. 
%  X         = fine mesh
%  Y,dY      = fem solution and its derivative on fine mesh X
%  X,Y,dY    are column vectors

tf=linspace(0,1,k+1); % fine mesh on basic element
[I,D]=basfnc(t,tf);

n=numel(a);  m=numel(t)-1;
Nf=(n-1)*k+1;     % number of all fine mesh points

X=zeros(Nf,1); 
Y=zeros(size(X));  
dY=zeros(size(X));
for l=1:n-1
  h=a(l+1)-a(l);  % size of element l
  nc=(l-1)*m;     % nc+j = global index of (coarse) point j on element l
  nf=(l-1)*k;     % nf+j = index of fine point j on element l   
  
  c=nc+(1:m+1);   % global coarse point indexes on element l    
  f=nf+(1:k+1);   % global fine points indexes on element l
  
  X(f)=linspace(a(l),a(l+1),k+1)';
  Y(f)=I*y(c);  
  dY(f)=D*y(c)/h;
end


