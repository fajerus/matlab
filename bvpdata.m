%% BVPDATA: set linear bvp data's 
% with exact solution
%

%--- BVP numeric data's -------------
a=0; b=1;        % integration domain
tbca=1; tbcb=3;  % type of bc condition
siga=1; sigb=2;  % if you need. 

%% BVP functional data
w=12;            % solution parameter
u = @(x) sin(w*x);    % exact solution in symbolic form
p = @(x) 1;
r = @(x) 0;
s = @(x) 0;
q = @(x) 1;
%%------------------------------------

%% Next, find the rest of the data
du=@(x) w*cos(w*x);        % du=u'
f =@(x) (w^2+1)*sin(w*x);  % rhs

ua=u(a);  ub=u(b);
if tbca==1
  sa=0; ga=ua;
else
  sa=sigb;
  ga=-(p(a)*du(a)+r(a)*u(a))+siga*ua;
end

if tbcb==1
  sb=0; gb=ub;
else
  sb=sigb; gb=(p(b)*du(b)+r(b)*u(b))+sigb*ub;
end

bc=[ tbca sa ga
     tbcb sb gb ];