clc
clear all
segm=[0,1];
m=10;
n=4;

t=linspace(0,1,n+1);

f=2*sin(2*pi*t);
y=spl(segm,f,1);
s=Spline(segm,f,y,m);

M=m*n+1;

x=linspace(segm(1),segm(2),M);
plot(x,s);