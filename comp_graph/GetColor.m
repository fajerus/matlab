function [ rgb ] = GetColor( MaxCol )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    t = linspace(0,1,MaxCol);
    rgb(:,1) = sqrt(t);
    rgb(:,2) = t .* t;
    rgb(:,3) = abs(t .* (t-0.5));
end

