%
nx = 800;
ny = 800;
tx = linspace(0, 1, nx+1);
ty = linspace(0, 1, ny+1);
MaxCol = 800;

nFig = figure(1);
%map = GetColor(MaxCol);

for i=1:nx+1
   x = 6*pi*tx(i);
   for j=1:ny+1
        y = 5*pi*ty(i);
        z(i,j) = x*exp(sin(x)+sin(y));
   end
end

zmin = min(z);
zmax = max(max(z));
%press = (MaxCol-1)/(zmax-zmin);

for i=1:nx+1
   for j=1:ny
      k = max(1, round(MaxCol*1/2*z(i,j)/zmax));
      c(i,j) = k;
   end
end


colormap('jet');
hC = image(c);