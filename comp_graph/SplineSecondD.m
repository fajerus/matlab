function [ y ] = SplineSecondD( segm, f, period )
% period:
% function func periodic or not
% a = segm(1), b = segm(2);
% F - row
% N = numel(f)-1 - length of 'f';
    a = segm(1); b = segm(2);
    n = numel(f) - 1;
    h = (b-a)/n;
    h_2 = 1/(h*h);


    if nargin == 2 % not periodic function
      e = ones(n-1,1);      
      A = spdiags( [e 4*e e ], -1:1, n-1, n-1);
      
      i=2:n;
      F = zeros(n-1,1);
      F = (f(i-1)-2 * f(i) + f(i+1) )' * h_2;
      
      
      y = A\F;
      
      y = [0, y', 0];
    else
      e = ones(n,1);
      A = spdiags( [e 4*e e ], -1:1, n, n);
      
      A(1,1) = 2; A(1,n) = 1;
      A(n,n) = 2; A(n,1) = 1; 
      
      F = zeros(n,1);
      i=1:n;
      
      
      y = A\F;
      
      y = [y', y(1)];
    end

    y = y * 1/6;
end