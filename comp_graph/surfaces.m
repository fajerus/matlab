%Tsurf1
clear all
clc

nx=100;
ny=150;
x=linspace(-1,1,nx);
y=linspace(-1,1,ny);

figure(1);
% tic
% for i=1:nx
%    for j=1:ny
%        z(j,i)=x(i)*sin(pi*x(i)*y(j));
%    end;
% end;
% mesh(x,y,z); %surf(x,y,z);
% t1=toc;
% ------------------------------------------
% figure(2);
tic
[xx,yy] = meshgrid(x,y);
zz=xx.*sin(pi*xx.*yy);
mesh(xx,yy,zz); %surf(xx,yy,zz);
t2=toc;
%ratio = t1/t2