%Tsurf2
clear all
clc

n=100;
a=0.1;
b=1;
c=0.1;

z=linspace(0,c,n);
phi=linspace(0,2*pi,n);
psi=linspace(-pi/2,pi/2,n);
[phi,z]=meshgrid(phi,z);

x=a*cos(phi);
y=b*sin(phi);



figure(1);
hA1 = axes;
mesh(x,y,z);

figure(2);
hA2 = axes;
surf(x,y,z);

set(hA1, 'YLim',[-1,1]);
set(hA2, 'YLim',[-1,1]);