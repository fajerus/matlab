nx = 100;
ny = 100;

for i=1:nx
   x = 4*pi*i/nx;
   for j=1:ny
      y = 3*pi*j/nx;
      z = sin(x) * sin(y);
      red = min( 1, ( sin(z)+1)/2 );
      blue = min( 1, ( 1-cos(z) )/2 );
      green = exp( sin(z) * sin(3*z) + sin(2*z-1) );
      green = min( 1, green );
      X(i,j,:) = [ red blue green];
   end
end

figure(1);
hX = image(X);