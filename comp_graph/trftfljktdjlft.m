clear all
clc

n=100;
a=1;
b=1;
c=1;
phi=linspace(0,2*pi,n);
psi=linspace(-pi/2,pi/2,n);
[phi,psi]=meshgrid(phi,psi);
cpsi=cos(psi);
x=a*cos(phi).*phi;
y=b*sin(phi).*phi;
z=c*sin(psi);
figure(2);
mesh(x,y,z);