%Tsurf2
clear all
clc
% �����
n=100;
a=1;
b=1;
c=1;

z=linspace(0,c,n);
phi=linspace(0,2*pi,n);
[phi,z]=meshgrid(phi,z);

x=a*z.*cos(phi);
y=b*z.*sin(phi);


figure(1);
hA1 = axes;
mesh(x,y,z);

set(hA1, 'YLim',[-1,1]);
