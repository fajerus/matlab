%surface5
clear all
clc

n=100;
a=4;
b=2;
c=1;

alpha = 6; %
betta = 3; %

z=linspace(0,c,n);
phi=linspace(0, 2*pi, n);
psi=linspace(-pi, pi, n);
[phi,psi]=meshgrid(phi,psi); 

cpsi= cos(psi);

x = a*cos(phi).*cpsi + alpha*cos(phi);
y = b*sin(phi).*cpsi + betta*sin(phi);
z = sin(psi);


lim = [-6,6];

figure(1);
hA1 = axes;
mesh(x,y,z);
for i=0:2:360
    view(i/2,i);
    set(hA1, 'YLim', lim);
    pause(1e-1);
end

