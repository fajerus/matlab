function y=spl(segm,f,z)
    a=segm(1);
    b=segm(2);
    n=numel(f)-1;
    h=(b-a)/n;
    N=n+1;
    F=zeros(N,1);
    
    if nargin==2

        e=ones(n-1,1);
        A=spdiags([e,4*e,e],-1:1,n-1,n-1);
        h2=1/(h*h);
        i=2:n;
        F=(f(i+1)-2*f(i)+f(i-1))'*h2;
        y=A\F;
        y=[0,y',0];

    else
        e=ones(n,1);
        A=spdiags([e,4*e,e],-1:1,n,n);
        A(1,1)=4; A(1,n)=1; 
        A(n,1)=1; A(n,n)=4;
        h2=1/(h*h);
        i=2:n;
        
        F(i)=(f(i+1)-2*f(i)+f(i-1))'*h2; %f(n+1)=f(1) !!
        F(1)=(f(2)-2*f(1)+f(n))*h2;
        %F(n)=(f(1)-2*f(n)+f(n-1))*h2;
        F(N)=[];
        y=A\F;
        y=[y',y(1)];
    end
end