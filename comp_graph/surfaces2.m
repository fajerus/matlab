%Tsurf2
clear all
clc

n=100;
a=1;
b=1;
c=1;
nC=flipud( linspace(0.1, c, n) );
% �������� �������� ������ ����

phi=linspace(0,2*pi,n);
psi=linspace(-pi/2,pi/2,n);
[phi,psi]=meshgrid(phi,psi);
cpsi=cos(psi);
x=a*cos(phi).*cpsi;
y=b*sin(phi).*cpsi;
z=c*sin(psi);

 figure(1);
 hA1 = axes;
 lim = [-1,1];
 set(hA1, 'XLim', lim, 'YLim', lim, 'ZLim', lim );
 for i=2:n
     
     z = nC(n+1-i)*sin(psi);
     hM = mesh(x,y,z);
     set(hA1, 'ZLim', lim);
     pause(1e-2);
 end
