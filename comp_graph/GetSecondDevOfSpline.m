%splain
clear all;
N = 200;
segm = [-2,2];
m = 100;
n=5;
x = linspace(segm(1),segm(2),n+1);

func = sin(pi*x);

y = SplineSecondD(segm, func);
s = Spline(segm, func, y, m);

M = m*n+1;
x2 = linspace(segm(1),segm(2),M);
func2 = sin(pi*x2);
plot(x2, s);
hold on;
%plot( x2, func2, 'Color', 'red' );

