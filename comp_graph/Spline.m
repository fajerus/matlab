function s=Spline(segm,f,y,m)
    n=numel(f)-1;
    h=(segm(2)-segm(1))/n;
    u=linspace(0,1,m+1);
    u(m+1)=[];
    v=1-u;
    h2=h*h;
    u3=u.*u.*u*h2;
    v3=v.*v.*v*h2;
    j=1:m;
    s=zeros(1,m*n+1);
    for i=1:n
        s(j)=v3*y(i)+u3*y(i+1)+v*(f(i)-y(i)*h2)+u*(f(i+1)-y(i+1)*h2);
        j=j+m;
    end
    s(n*m+1)=f(n+1);
end