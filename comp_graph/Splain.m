function varargout = Splain(varargin)
% SPLAIN MATLAB code for Splain.fig
%      SPLAIN, by itself, creates a new SPLAIN or raises the existing
%      singleton*.
%
%      H = SPLAIN returns the handle to a new SPLAIN or the handle to
%      the existing singleton*.
%
%      SPLAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPLAIN.M with the given input arguments.
%
%      SPLAIN('Property','Value',...) creates a new SPLAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Splain_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Splain_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Splain

% Last Modified by GUIDE v2.5 19-Nov-2014 15:29:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Splain_OpeningFcn, ...
                   'gui_OutputFcn',  @Splain_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Splain is made visible.
function Splain_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Splain (see VARARGIN)

% Choose default command line output for Splain
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Splain wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Splain_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Draw.
function Draw_Callback(hObject, eventdata, handles)
% hObject    handle to Draw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Rang=get(handles.Range,'String');
Rang=str2num(Rang);
Freq=str2num(get(handles.Frequency,'String'));
Freq=Freq+1;
h=(Rang(2)-Rang(1))/Freq;
x = Rang(1):h:Rang(2);%������ �����
str_func=get(handles.Function,'String');
y=eval(str_func);%������ ��������


n=length(x);
A=zeros(n-2,n-2);%������. �������
for k=1:n-2
    for p=1:n-2
        if k==p
            A(k,p)=4;
        elseif (k==p+1 || k==p-1)
            A(k,p)=1;
        else
            A(k,p)=0;
        end
    end
end
 
 
del = diff(y);
del2 = diff(del);
 
b = inv(A)*del2'*3/(h^2);%���������� ����-�� b
b = [0;b;0] ;% b(1) = 0 �.�. S0=0 � b(n) = 0 �.�. Sk''(xn)=0
a = diff(b)/(3*h);%���������� ����-�� �
 
c=zeros(1,n-1);
for k=1:n-1
    c(k)=del(k)/h-(b(k+1)+2*b(k))*h/3;%���-� ����-�� c
end
hold on;
 
for k=1:n-1
    T = 0:h/10:h;%���������� �������
    g = a(k)*T.^3 + b(k)*T.^2 + c(k)*T + y(k);
    plot(x(k)+T, g, 'g');
end









% Rang=str2num(get(handles.Range,'String'))
% Freq=str2num(get(handles.Frequency,'String'))
% x=linspace(Rang(1),Rang(2),Freq+1);
% F=eval(get(handles.Function,'String'));
% c(1)=0;
% c(Freq+1)=0;
% h=x(2)-x(1);
% alpha(1)=0;
% beta(1)=0;
% for i=2:Freq
%     Koef=6*((F(i+1)-F(i))/h-(F(i)-F(i-1))/h);
%     z=(h*alpha(i-1)+4*h);
%     alpha(i)=-h/z;
%     beta(i)=(Koef-h*beta(i-1)/z);
% end;
% for i=Freq :2
%     c(i)=alpha(i)*c(i+1)+beta(i);
% end;
% d(Freq+1)=0;
% b(Freq+1)=0;
% for i=Freq+1:2
%     d(i)=(c(i)-c(i-1))/h;
%     b(i)=h*(2*c(i)+c(i-1))/6+(F(i) - F(i-1)) / h;
% end;
% N=10*(Freq+1)
% nx=linspace(Rang(1),Rang(2),N);
% y=[];
% for p=1:N
%     i=1;
%     j=Freq+1;
%     k=0;
%     while (i+1<j)
%         k=ceil(i+(j-i)/2);
%         if (nx(p)<=x(k))
%             j=k;
%         else
%             i=k;
%         end;
%     end;
%     dx=nx(p)-x(j);
%     y=[y,F(j)+(b(j)+(c(j)/2+d(j)*dx/6)*dx)*dx];
% end;
% plot(nx,y);

        

% hold on;
% z1=[];
% y1=[];
% for i=2:Freq+1
%     z=linspace(x(i-1),x(i),10);
%     for j=1:10        
%         y=F(i)+b(i)*(z(j)-x(i-1))+(c(i)*(z(j)-x(i-1))^2)/2+(d(i)*(z(j)-x(i-1))^3)/6;
%     end;
%     plot(z,y);
%     z1=[z1,z];
%     y1=[y1,y];       
% end;
% length(z1)
% length(y1)
% %plot(z1,y1,'-');
% plot(x,F); 
% hold off;


function Function_Callback(hObject, eventdata, handles)
% hObject    handle to Function (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Function as text
%        str2double(get(hObject,'String')) returns contents of Function as a double


% --- Executes during object creation, after setting all properties.
function Function_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Function (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Range_Callback(hObject, eventdata, handles)
% hObject    handle to Range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Range as text
%        str2double(get(hObject,'String')) returns contents of Range as a double


% --- Executes during object creation, after setting all properties.
function Range_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Frequency_Callback(hObject, eventdata, handles)
% hObject    handle to Frequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Frequency as text
%        str2double(get(hObject,'String')) returns contents of Frequency as a double


% --- Executes during object creation, after setting all properties.
function Frequency_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Frequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Clear.
function Clear_Callback(hObject, eventdata, handles)
% hObject    handle to Clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cla;
