function [x,w] = qgauss(n,a,b)
% QGAUSS: find nodes and weights of n-point 
% Gauss quadrature rule on [a,b].  
% x = nodes=zeroes of Legendre polynomial L_n(x) on [a,b]
% w = weights. 
% x,w  are column vectors

if n==1, x=0; w=2; return; end

j=1:n-1;
beta=j./sqrt(4*j.^2-1);

A=diag(beta,-1)+diag(beta,1);

% Find nodes x and weights w for [-1,1].
[V,D]=eig(A);
[x,k]=sort(diag(D));
w=2*V(1,k).^2;
w=w(:);

% nodes and weights on [a,b]
x=a+0.5*(b-a)*(x+1);
w=0.5*(b-a)*w;