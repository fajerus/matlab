% main file
clc; clear all; close all

%-----------------------------------------------------
bvpdata;        % set ODE and bc data 

% --- set fem data -----------------------------------
m =30;                 % polynomial degree  
ne=1;                  % number of f.e., n=ne+1  
xa=linspace(a,b,ne+1); % [a,b]=sum of e_i=[xa_i,xa_{i+1}] 
M =m+1;                % number of quadrature  nodes
[t,d,c]=baselm(m,M,'lob','lob'); % basic element data's

%---- solve bvp   -------------------------------------
tic   % solve bvp
[y,x] = linbvp(bc,p,r,s,q,f,xa,t,d,c); % y=fem solution on mesh x
timeFemSol=toc

%--- find error in L_2 and H^1 norm -------------------
[e0,e1]=femerr(u,du,y,xa,t)

[X,Y,dY]=finesol(y,xa,t,10*m);  % Y=solution on the fine grid X

%--- find error in different max norm -------------------
Z=u(X)-Y;           % u-uh   on grid X
z=u(x)-y;           % u-uh   on grid x 
dZ=du(X)-dY;        % u'-uh' on grid X

% error in max-norm
ie=1:m:numel(x);  % indexes of elements boundary nodes
einfh=norm(Z,inf)
einfdh=norm(dZ,inf)
einfbh=norm(u(xa(:))-y(ie),inf)

%---- plot solution -------------------------------------
figure;  % plot solution
plot(X,Y,'-b',x,0,'kx',x(ie),0,'ks',x,y,'rx')
xlabel('x');  ylabel('u_h')

figure;% plot solution error
plot(X,Z,'-b',x,z,'kx',x(ie),z(ie),'ks')
xlabel('x'); ylabel('u-u_h')

figure;% plot derivatives error
plot(X,dZ,'-b',x,zeros(size(x)),'rx',x(ie),zeros(size(x(ie))),'rs')
xlabel('x'); ylabel('u''-u''_h')