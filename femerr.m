function [e0,e1]=femerr(u,du,y,a,t)
%% FEMERR: find fem solution error in different norms 
%  u, du     = pointers to functions of exact solution u and u'  
%  y(1:N)    = fem solution on mesh x
%  a(1:n)    = f.e. boundary points
%  t(1:m+1)  = grid points on basic element [0,1]
%  e0=\|u-u_h\|_L_2,   e1=\|u-u_h\|_H^1_0,

m=numel(t)-1;

[d,c] = qgauss(m+2,0,1); % new quadrature on [0,1] 
[I,D] = basfnc(t,d);

e0=0; e1=0; 
for l=1:numel(a)-1
  h =a(l+1)-a(l); % size of element l
  dl=a(l)+h*d;    % quadrature nodes on e_l
  nc=(l-1)*m;     % nc+j = global index of  point j on element l
  ic =nc+(1:m+1); % global point indexes on element l    
   
  uh =I*y(ic);    duh=D*y(ic)/h;
  e  = u(dl)-uh;  de = du(dl)-duh; 
    
  e0=e0 + h/2*sum(c.*e.^2); 
  e1=e1 + h/2*sum(c.*de.^2); 
end
e0=e0^(1/2); e1=e1^(1/2);