function [t,d,c]=baselm(m,M,typet,typeq)
%% BASELM: set basic element data
% m     = polynomial degree
% M     = number of quadrature nodes
% typet = type of distribution of mesh points 
% typeq = type of quadrature rule

switch lower(typet)
   case {'lin', 'linear'}
      t=linspace(0,1,m+1);
   case {'c', 'ch', 'chb', 'cheb'}
      t=sort((cos(pi*(0:m)/m)+1)/2);
   case {'l', 'lo', 'lob', 'lobat', 'lobatto'}
      t=qlob(m+1,0,1);
   otherwise
      t=qlob(m+1,0,1);
end
switch lower(typeq)
   case {'g', 'ga', 'gau', 'gaus', 'gauss'}
      [d,c]=qgauss(M,0,1);
   case {'l', 'lo', 'lob', 'lobat', 'lobatto'}
      [d,c]=qlob(M,0,1);   
   otherwise
      [d,c]=qlob(M,0,1);
end