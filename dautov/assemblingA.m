function [ A ] = assemblingA( p, q, r, x )
n = numel(x);
A = sparse(n, n);
h = x(2)-x(1);


for k=1:n-1
    m=k:k+1;
    Ak = zeros(2,2);
    for alp = 1:2
        for beta = 1:2
            Ak(alp, beta) = h * ( p( x(k) + h/2 )*(-1)^(alp+beta)/h^2 + ...
                q( x(k)+h/2 ) * ( (-1)^beta )/(2*h) + ... 
                r( x(k) + h/2 )/4 );
        end
    end
    A(m,m) = A(m,m) + Ak;
end


end

