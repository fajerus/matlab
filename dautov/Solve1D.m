function y = Solve1D(p,q,r,f,bc,x)
n=numel(x);
h=x(2)-x(1);

A=sparse(n,n);
F=zeros(n,1);
for i=2:n-1
    A(i,i-1)=-p(x(i)-h/2)/(h^2)-q(x(i))/(2*h);
    A(i,i)=(p(x(i)+h/2)+p(x(i)-h/2))/h^2+r(x(i));
    A(i,i+1)=-p(x(i)+h/2)/(h^2)+q(x(i))/(2*h);
    F(i)=f(x(i));
end

% if left boundary contain 1st bc
if(bc(1,1)==1)
    A(1,1)=1;
    F(1)=bc(1,3);
end
% if left boundary contain 3rd bc
if(bc(1,1)==3)
    sa=bc(1,2);
    ga=bc(1,3);
    A(1,1)=p(x(1)+h/2)/h+sa-q(x(1))/2+r(x(1))*h/2;
    A(1,2)=q(x(1))/2-p(x(1)+h/2)/h;
    F(1)=f(x(1))*h/2+ga;    
end
% if right boundary contain 1st bc
if(bc(2,1)==1)
	A(n,n)=1;
	F(n)=bc(2,3);
end
% if right boundary contain 3rd bc
if(bc(2,1)==3)
     sb=bc(2,2);
     gb=bc(2,3);
     A(n,n-1)=(-q(x(n))/2+p(x(n)-h/2)/h);
     A(n,n)=-p(x(n)-h/2)/h+sb+q(x(n))/2+r(x(n))*h/2;
     F(n)=f(x(n))*h/2+gb;
%     an=h*F(n-1)/2+gb+(q(x(n+1))/2-p(x(n+1)-h/2)/h)*y(n-1);
   
end
y=A\F;

end

