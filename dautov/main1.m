clear all;
close all;
clc; 

a = 0; b = 1;

k = 2;
u  = @(z) sin(k*z);
du = @(z) k*cos(k*z);
p  = @(z) 1;
q  = @(z) 0;
r  = @(z) 0;
f  = @(z) k*k*sin(k*z);

sa = 2;
sb = 5;
bc = [3, sa, -p(a)*du(a)+sa*u(a)
      3, sb, -p(b)*du(b)+sb*u(b)];

n=100;
x=linspace(a,b,n); x = x';
y = Solve1D(p,q,r,f,bc,x);

figure;
plot(x,y,'-b',x,u(x),'-r') 
xlabel('x')
legend('y','u')

figure;
plot(x,y-u(x),'-b')
xlabel('x')
ylabel('error=y-u')