clear all;
close all;
clc; 

a = 0; b = 1;

k = 2;
u  = @(z) sin(k*z);
du = @(z) k*cos(k*z);
p  = @(z) 1;
q  = @(z) 0;
r  = @(z) 0;
f  = @(z) k*k*sin(k*z);

% bc = [1, 0, u(a)
%       1, 0, u(b)];
sa = 2;
sb = 5;
bc = [3, sa, -p(a)*du(a)+sa*u(a)
      3, sb, -p(b)*du(b)+sb*u(b)];

n=100;
x=linspace(a,b,n)';
A = assemblingA(p,q,r,x);
A
