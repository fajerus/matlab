clear all
close all
clc

a = 0; b = 2*pi;
n = 100;
x = linspace(a, b, n);
h = x(2)-x(1);
k = 3;

p = @(x) 1;
q = @(x) 0;
r = @(x) 0;

Ua = 0;
Ub = 0;

bc = [ 1 0 Ua;
       1 0 Ub];

u = @(x) sin(k * x);
f = @(x) -k*k * sin(k * x);

fx = f(x);
% set boundary conditions
% fx(1) = fx(1) * h/2 + bc(1,3);
% fx(n) = fx(n) * h/2 + bc(2,3);

A = matrixAssembler(p, q, r, x);

y = A \ fx';

plot(x, u(x));
hold on
plot(x, y);