function [ A ] = matrixAssembler( p, q, r, x )
  n = size(x, 2);
  h = abs(x(2) - x(1));
  A = sparse(n, n);

  h2  = h * 2;
  h_2 = h / 2;
  hh = h * h;

  for i=2:n
    A(i-1, i) = p( x(i)- h_2 ) / hh - q(x(i)) / h2; % ai
  end

  for i=1:n
    A(i, i) = p( x(i)+h_2 ) / hh + p( x(i)-h_2 )/hh + r( x(i) );
  end
  
  for i=1:n-1
    A(i, i+1) = - p( x(i) + h_2 ) / hh + q( x(i) ) / h2;
  end

end

