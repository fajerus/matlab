function [K,F] = assemKF(p,r,s,q,f,a,t,d,c)
%% ASSEMKF: Assembles element contributions to FEM system.
% Stiffness matrix K and force vector F corresponds to the equation 
%       -(pu'+ru)'+su'+qu=f,  a1<x<b1,
% and homogenous Neumann bondary conditions  pu'+ru=0.
% input data are commented in LINBVP
%%
[v,dv]=basfnc(t,d); u=v'; du=dv'; % matrices I, D, I^T, D^T

m=numel(t)-1;  n=numel(a);  N=(n-1)*m+1;      

K=sparse(N,N);    % stiffness matrix
F=zeros(N,1);     % rhs
for l=1:n-1
  h =a(l+1)-a(l); % size of element l  
  nl=(l-1)*m;     % nl+j = global index of point j on element l  
  ne=nl+(1:m+1);  % global point indexes on element l  
  xd =a(l)+h*d;   % quadrature nodes on element l
  
  P=diag((c/h).*p(xd));
  R=diag(  c.*r(xd));
  S=diag(  c.*s(xd));
  Q=diag((h*c).*q(xd));  
    
  Kl=(du*P+u*S)*dv + (du*R+u*Q)*v; 
  K(ne,ne)=K(ne,ne)+Kl;
  
  Fl=(h*c).*f(xd);
  F(ne)=F(ne) + u*Fl(:);   
end