function [I,D]=basfnc(t,d)
%% BASFNC find interpolation (I) and differentiation matrix (D).
% t = interpolation points
% d = arbitrary nodes
% I ={\phi_j (d_i), j=1,..,n, i=1,...,N},
% D ={\phi'_j(d_i), j=1,..,n, i=1,...,N},
% \phi_j=Lagrange basis functions: \phi_j(t_i)=\delta_ij.

n=numel(t); N=numel(d);
I =zeros(N,n); D =zeros(N,n);
b=zeros(1,n);
for i=1:n
  j=[1:(i-1),(i+1):n];
  b(i)=1/prod(t(i)-t(j));
end
for i=1:n
  j=[1:(i-1),(i+1):n];
  for k=1:N
    I(k,i)=b(i)*prod(d(k)-t(j));    % \phi_i(d_k)
    ds=0;
    for s=j
      i1=min(i,s); i2=max(i,s); 
      jj=[1:(i1-1),(i1+1):(i2-1), (i2+1):n];
      ds=ds + prod(d(k)-t(jj));              
    end
    D(k,i)=b(i)*ds;                 % \phi'_i(d_k)
  end  
end

