function [y,x] = linbvp(bc,p,r,s,q,f,a,t,d,c)
% LINBVP: Solve by FEM a BVP for 2-nd order ODE 
%       -(pu'+ru)'+su'+qu=f,  a<x<b.
% with boundary conditions: 
%     u(a)=ua (type=1)  OR   -(pu'+ru)(a)+siga u(a)=ga (type=3)
%     u(b)=ub (type=1)  OR    (pu'+ru)(b)+sigb u(b)=gb (type=3).
%
%  bc   = boundary conditions matrix of size 2x3: 
%         bc(1,:)=[1, 0, ua], OR  bc(1,:)=[3, siga, ga]
%         bc(2,:)=[1, 0, ub], OR  bc(2,:)=[3, sigb, gb]
%
%  p,r,s,q,f = pointers of vectorized functions of one variable x
%  a(1:n)    = f.e. boundary points
%  t(1:m+1)  = mesh points on basic element [0,1],
%              m=degree polynomial on each finite element 
%  (d,c)(1:M)= quadrature nodes and weights on basic element [0,1]
%
%  y = fem solution on mesh x 
%  y,x are column vectors  

m=numel(t)-1; n=numel(a);
N=(n-1)*m+1;          % number of mesh points

x=zeros(N,1);         % all mesh points
for l=1:n-1
  nl=(l-1)*m;         % nl+j = global index of point j on element l
  ne=nl+(1:m+1);      % global point indexes on element l
  x(ne)=a(l)+(a(l+1)-a(l))*t;  % mesh points on element l
end

[K,F] = assemKF(p,r,s,q,f,a,t,d,c);
[K0,F0] = assemBC(bc,K,F);
y = solveLbvp(bc,K0,F0);