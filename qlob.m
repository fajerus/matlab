function [x,w] = qlob(n,a,b)
% QLOB: find nodes and weights of 
% n-point Lobatto quadrature rule on [a,b].
% x = nodes =zeros of (1-t^2)L'_{n-1}(t) shifted on [a,b],
% w = weights.
% x,w  are column vectors

if n<2, 
  disp('QLOB: number of Lobatto nodes less then 2'); return; 
end
if nargin==1, a=-1; b=1; end
if n==2, x=[a;b]; w=[(b-a)/2;(b-a)/2]; return; end

% Find nodes x and weights w of (n-2) point
% Gauss-Jacoby quadrature with weight 1-x^2 on [-1,1] 

k=1:n-3;
beta=sqrt(k.*(k+2)./(2*k+1)./(2*k+3));

A=diag(beta,-1)+diag(beta,1);

[V,x]=eig(A);
[x,k]=sort(diag(x));
w=4/3*(V(1,k).^2)';

% Lobatto nodes and weights on [-1,1]
w=[2/(n^2-n); w./(1-x.^2); 2/(n^2-n)];
x=[-1; x; 1];

% Lobatto nodes and weights on [a,b]
x=a+0.5*(b-a)*(x+1);
w=0.5*(b-a)*w;