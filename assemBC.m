function [K0,F0] = assemBC(bc,K,F)
%% ASSEMBC: Assembles boundary conditions to 
% stiffness matrix (K) and rhs (F)
% bc = boundary conditions matrix is commented if LINBVP

N=numel(F);  % number of all mesh points

% set boundary conditions of 3-d type
if bc(1,1)==3
   K(1,1)=K(1,1)+ bc(1,2);
   F(1)  =F(1)  + bc(1,3);
end
if bc(2,1)==3
   K(N,N)=K(N,N)+ bc(2,2);
   F(N)  =F(N)  + bc(2,3);
end

% set boundary conditions of 1 type
ib=1; ie=N; 
if bc(1,1)==1, ib=2;   ua=bc(1,3); F=F-ua*K(:,1); end
if bc(2,1)==1, ie=N-1; ub=bc(2,3); F=F-ub*K(:,N); end

I=(ib:ie)';
K0=K(I,I); 
F0=F(I);
