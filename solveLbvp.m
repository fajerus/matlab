function y = solveLbvp(bc,K0,F0)
%% SOLVELBVP: Solve by FEM a linear BVP for 2-nd order ODE   
%  bc   = defined in ASSEMBC 
%  K0, F0    = resulting stiffness matrix and rhs of fem system
%  y = fem solution (column vector)

% get Dirichlet data
ua=[]; ub=[];
if bc(1,1)==1, ua=bc(1,3); end
if bc(2,1)==1, ub=bc(2,3); end

u0=K0\F0; 
y=[ua; u0; ub];  